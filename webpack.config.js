
const path = require('path');
var nodeExternals = require('webpack-node-externals');
var webpack = require('webpack');

module.exports = {
    entry: {
        server: './server.ts'
    },
    resolve: {
        extensions: ['.ts', '.js'],
        alias: {
            'main.server': path.join(__dirname, 'dist', 'server', 'main.bundle.js')
        }
    },
    target: 'node',
    externals: [nodeExternals()],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].js'
    },
    module: {
        rules: [
            { test: /\.ts$/, loader: 'ts-loader' }
        ]
    },
    plugins: [
        // Temporary Fix for issue: https://github.com/angular/angular/issues/11580
        // for 'WARNING Critical dependency: the request of a dependency is an expression'
        new webpack.ContextReplacementPlugin(
          /(.+)?angular(\\|\/)core(.+)?/,
          path.join(__dirname, 'src'), // location of your src
          {} // a map of your routes
        ),
        new webpack.ContextReplacementPlugin(
          /(.+)?express(\\|\/)(.+)?/,
          path.join(__dirname, 'src'),
          {}
        )
      ]
}