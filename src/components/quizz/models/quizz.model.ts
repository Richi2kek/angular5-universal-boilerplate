import Question from './questions.model';

class Quizz {
    id: number;
    title: string;
    questions: Question[];

    constructor(
        title?,
        questions?,
        id?
    ) {
        this.title = title || '';
        this.questions = questions || [];
        this.id = id || null;
    }
}

export default Quizz;