import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';

import { FormGroup, FormControl, Validators} from '@angular/forms';

import Quizz from '../models/quizz.model';
import Question from '../models/questions.model';
@Component({
    selector: 'app-component-quizz-form',
    templateUrl: 'quizz-form.component.html',
    styleUrls: [
        'quizz-form.component.scss'
    ]
})

export class QuizzFormComponent implements OnInit, OnChanges {

    @Input() quizz: Quizz = new Quizz();
    private questions: any[];

    constructor() {
    }

    ngOnInit() {
    }

    ngOnChanges() {

    }

    handleAddButton() {
        this.quizz.questions.push(new Question(this.quizz.questions.length + 1));
    }

    handleAddOptionButton(options: string[]) {
        options.push('');
    }

    handleUpButton(question: Question) {
        this.quizz.questions[question.position - 1] = this.quizz.questions[question.position - 2];
        this.quizz.questions[question.position - 2] = question;
        this.resetQuestionsPosition();
    }

    handleDownButton(question: Question) {
        this.quizz.questions[question.position - 1] = this.quizz.questions[question.position];
        this.quizz.questions[question.position] = question;
        this.resetQuestionsPosition();
    }

    handleDeleteButton(question: Question) {
        this.quizz.questions = this.quizz.questions.filter(question => question !== question);
        this.resetQuestionsPosition();
    }

    handleDeleteOptionButton(options: string[]) {
        if (options.length > 1) {
            options.splice(options.length - 1, 1);
        }
        this.resetQuestionsPosition();
    }

    resetQuestionsPosition() {
        this.quizz.questions.forEach((question, i ) => {
            question.position = i + 1;
        });
    }

}
