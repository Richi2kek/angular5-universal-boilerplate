import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';

import { AppComponent } from './app.component';

import { HomeComponent } from '../pages/home/home.component';
import { DashboardComponent } from '../pages/dashboard/dashboard.component';
import { NotFoundComponent } from '../pages/not-found/not-found.component';

import { QuizzFormComponent } from '../components/quizz/quizz-form/quizz-form.component';
import { QuizzRenderComponent } from '../components/quizz/quizz-render/quizz-render.component';
import { QuizzEditableComponent } from '../components/quizz/quizz-editable/quizz-editable.component';
import { HttpClient } from 'selenium-webdriver/http';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    NotFoundComponent,

    QuizzFormComponent,
    QuizzRenderComponent,
    QuizzEditableComponent

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'server-side' }),
    BrowserTransferStateModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,

    AppRoutingModule,
    AppMaterialModule,

  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
