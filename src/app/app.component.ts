import { Component } from '@angular/core';

import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'My App';
  isSidenavOpened: boolean = false;

  theme: string;
  style: any;

  constructor(
  ) {
    this.theme = 'default-light-theme';
    this.style = {
      background: 'lightgrey',
    };
  }

  handleMenuButton() {
    this.isSidenavOpened = !this.isSidenavOpened;
  }

  onSidenavChange(event) {
    this.isSidenavOpened = event;
  }

  handleThemeButton() {
    this.theme = this.theme === 'default-light-theme' ?
      'default-dark-theme' :
      'default-light-theme';

    this.style = this.theme === 'default-dark-theme' ?
      {
        background: '#212121'
      } :
      {
        background: 'lightgrey'
      };
  }
}
