# AngularV5 server side rendering
- `npm install`

## Serve dev
- `npm start`
- go to `localhost:4200`

## Build prod
- `npm run build`
- `node dist/server.js`
- go to `localhost:4000`
